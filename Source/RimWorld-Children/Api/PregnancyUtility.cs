﻿namespace RimWorldChildren.API {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using RimWorld;
    using Verse;
    using RimWorldChildren;

    /// <summary>
    /// Static utility methods for pregnancy related operations.
    /// </summary>
    public static class PregnancyUtility {

        private static List<TraitDef> geneticTraits = new List<TraitDef>();
        private static Dictionary<Type, string> hediffOfClass = null;

        static PregnancyUtility() {
            // Genetic Traits
            RegisterGeneticTrait(TraitDefOf.Psychopath);
            RegisterGeneticTrait(TraitDefOf.SpeedOffset);
            RegisterGeneticTrait(TraitDefOf.Tough);
            RegisterGeneticTrait(TraitDefOf.Beauty);
            RegisterGeneticTrait(TraitDefOf.Industriousness);
            RegisterGeneticTrait(TraitDefOf.TooSmart);
            RegisterGeneticTrait(TraitDefOf.DrugDesire);
            RegisterGeneticTrait(TraitDefOf.NaturalMood);
            RegisterGeneticTrait(TraitDefOf.Nerves);
            RegisterGeneticTrait(TraitDefOf.PsychicSensitivity);
            RegisterGeneticTrait(TraitDefOf.AnnoyingVoice);
            RegisterGeneticTrait(TraitDefOf.CreepyBreathing);
            RegisterGeneticTrait(TraitDefOf.GreatMemory);
            RegisterGeneticTrait(TraitDefOf.Greedy);
            RegisterGeneticTrait(TraitDefOf.Jealous);
            RegisterGeneticTrait(TraitDefOf.Kind);
            RegisterGeneticTrait(TraitDef.Named("Immunity"));
            RegisterGeneticTrait(TraitDef.Named("Neurotic"));
            RegisterGeneticTrait(TraitDef.Named("QuickSleeper"));
            RegisterGeneticTrait(TraitDef.Named("Wimp"));
        }

        /// <summary>
        /// A lookup table for Hediff Classes to HediffDef names.
        /// Used to identify the associated hediff def when referencing a class.
        /// Additional classes may be added manually or by using the AssociatedHediff annotation.
        /// </summary>
        public static Dictionary<Type, string> HediffOfClass {
            get {
                if (hediffOfClass == null) {
                    hediffOfClass = new Dictionary<Type, string>();
                    var childClasses = AppDomain.CurrentDomain.GetAssemblies()
                        .SelectMany(
                            a => a
                            .GetTypes()
                            .Where(t => t.IsSubclassOf(typeof(Hediff_Pregnancy)))
                        );

                    foreach (var pregClass in childClasses) {
                        var attribute = (AssociatedHediff)pregClass.GetCustomAttributes(typeof(AssociatedHediff), false).FirstOrDefault();
                        if (attribute != null) {
                            hediffOfClass[pregClass] = attribute.defName;
                        }
                    }
                }

                return hediffOfClass;
            }
        }

        /// <summary>
        /// Register a trait to have it considered a genetically inheritable trait passed
        /// from parent to child. Pawns may be born with these traits.
        /// </summary>
        /// <param name="def">New trait to be added</param>
        public static void RegisterGeneticTrait(TraitDef def) {
            if (!geneticTraits.Contains(def)) {
                geneticTraits.Add(def);
            }
        }

        /// <summary>
        /// Retrieve a list of genetic traits
        /// </summary>
        /// <returns>a list of genetic traits</returns>
        public static List<TraitDef> GetGeneticTraits() {
            return new List<TraitDef>(geneticTraits);
        }

        /// <summary>
        /// Checks if a given traitdef is listed as a genetic trait
        /// </summary>
        public static bool IsGeneticTrait(TraitDef def) {
            return geneticTraits.Contains(def);
        }

        /// <summary>
        /// Determines if two pawns can breed based on a number of factors.
        /// </summary>
        /// <param name="pawn">A male or female pawn</param>
        /// <param name="partner">A male or female pawn</param>
        /// <returns>True if both pawns are capable of producing offspring</returns>
        public static bool CanPawnsMate(Pawn pawn, Pawn partner) {
            if (pawn.gender == partner.gender) {
                return false;
            }

            if (pawn.health.hediffSet.HasHediff(HediffDef.Named("Sterile")) || partner.health.hediffSet.HasHediff(HediffDef.Named("Sterile"))) {
                return false;
            }

            Pawn male = pawn.gender == Gender.Male ? pawn : partner;
            Pawn female = pawn.gender == Gender.Female ? pawn : partner;

            // Check Mod Settings to ensure pregnancy isn't disabled
            if (!ChildrenAndPregnancy.Settings.RacialSettings(female.def.defName).pregnancyEnabled) {
                return false;
            }

            if (female.health.hediffSet.HasHediff(HediffDef.Named("PostPartum"))) {
                return false;
            }

            if (!ChildrenUtility.RaceUsesChildren(female)) {
                return false;
            }

            if (IsPregnant(female) || IsOnContraceptive(female) || IsOnContraceptive(male)) {
                return false;
            }

            return true;
        }

        /// <summary>
        /// This method will check conditions to determine if it as acceptable for a new pawn baby to be added to the colony.
        /// </summary>
        /// <returns>True if all conditions for adding a new baby to the colony are true.</returns>
        public static bool ColonyCanGiveBirth() {
            // Check if population limit applies
            if (ChildrenAndPregnancy.Settings.populationCap > 0 && PawnsFinder.AllMapsCaravansAndTravelingTransportPods_Alive_OfPlayerFaction_NoCryptosleep.Count >= ChildrenAndPregnancy.Settings.populationCap) {
                return false;
            }

            return true;
        }

        /// <summary>
        /// This method will attempt to determine if a pawn is already pregnant at any stage.
        /// </summary>
        /// <param name="pawn">A pawn of any type and gender</param>
        /// <returns> true only if the pawn has a hediff with the string "pregnant" in the definition</returns>
        public static bool IsPregnant(Pawn pawn) {
            BodyPartRecord torso = pawn.RaceProps.body.AllParts.Find(x => x.def == BodyPartDefOf.Torso);

            // Attempt to find other hediffs that might indicate pregnancy too
            return pawn.health.hediffSet.HasHediff(HediffDefOf.Pregnant, torso) || pawn.health.hediffSet.hediffs.Find(hediff => hediff.def.defName.ToLower().Contains("pregnant")) != null;
        }

        /// <summary>
        /// This method will attempt to determine if a pawn is using any form of contraceptive.
        /// </summary>
        /// <param name="pawn">Any type of pawn</param>
        /// <returns>True if the pawn has a contraceptive hediff</returns>
        public static bool IsOnContraceptive(Pawn pawn) {
            HediffDef contraceptive = HediffDef.Named("Contraceptive");
            return pawn.health.hediffSet.HasHediff(contraceptive, null);
        }

        /// <summary>
        /// This method will attempt to impregnate pawn assuming that partner is the father.
        /// It requires a valid subclass of Hediff_Pregnancy, and is essentially just a
        /// helper method to call Hediff_Pregnancy.Create<T>() without a CNP dependency.
        /// </summary>
        /// <typeparam name="T">Hediff_Pregnancy subclass type</typeparam>
        /// <param name="pawn">The pawn to impregnate.</param>
        /// <param name="partner">The pawn's partner. Assumed to be the father.</param>
        /// <returns>If successful, returns the instance of Hediff_Pregnancy added to pawn.</returns>
        public static Hediff_Pregnancy ImpregnatePawn<T>(Pawn pawn, Pawn partner) {
            if (!typeof(T).IsSubclassOf(typeof(Hediff_Pregnancy))) {
                Log.Error("Attempting to pass an invalid class to ImpregnatePawn. Provided type must be subclass of Hediff_Pregnancy.");
                return null;
            }

            return Hediff_Pregnancy.Create<T>(pawn, partner);
        }
    }
}
