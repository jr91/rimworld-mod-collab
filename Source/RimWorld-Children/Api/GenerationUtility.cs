﻿using System.Collections.Generic;
using Verse;

namespace RimWorldChildren.API {
    /// <summary>
    /// Utilities for child generation rules.
    /// </summary>
    public static class GenerationUtility {

        private static HashSet<HediffDef> hediffWhitelist = new HashSet<HediffDef>();

        public static HashSet<HediffDef> HediffWhitelist {
            get {
                return new HashSet<HediffDef>(hediffWhitelist);
            }
        }

        public static void RegisterHediffToWhitelist(HediffDef def) {
            hediffWhitelist.Add(def);
        }
    }
}
