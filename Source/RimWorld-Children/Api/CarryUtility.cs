﻿namespace RimWorldChildren.Api {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using UnityEngine;
    using Verse;

    public class CarryUtility {

        /// <summary>
        /// Determines if a pawn is currenly being carried.
        /// </summary>
        /// <param name="pawn">The pawn to check</param>
        /// <returns>True if the pawn currently has the carried job</returns>
        public static bool IsCarried(Pawn pawn) {
            return pawn.CurJob != null && pawn.CurJob.def == CnpJobDefOf.Carried;
        }

        /// <summary>
        /// Adjusts the rendering layer for a passed in vector if the pawn is currently being carried.
        /// </summary>
        /// <param name="vector">Any vector</param>
        /// <returns>The original vector if the pawn is not carried, or a modified Y value if carried</returns>
        public static Vector3 AdjustCarryLayer(Vector3 vector, Pawn pawn) {
            if (CarryUtility.IsCarried(pawn)) {
                // Adjust the draw depth of the carried pawn so they appear in front of or behind the carrier as needed
                if (pawn.TryGetComp<CompCarryChild>().Carrier.Rotation == Rot4.North) {
                    vector.y -= 1f;
                } else {
                    vector.y += 1f;
                }
            }

            return vector;
        }
    }
}
