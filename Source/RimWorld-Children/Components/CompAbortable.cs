﻿namespace RimWorldChildren {
    using RimWorld;
    using Verse;

    /// <summary>
    /// Defines a pregnancy component that allows a pregnancy to be aborted
    /// </summary>
    public class CompAbortable : HediffComp {

        public CompProperties_Abortable Props => (CompProperties_Abortable)props;

        /// <summary>
        /// Aborts a specific baby, should be repeated for each baby to be removed.
        /// </summary>
        /// <param name="baby"></param>
        /// <param name="messageKey">a message bundle key to provide the translated message. May be left null to send no message.</param>
        /// <param name="messageArgs">a collection of arguments to be passed for formatting the provided message</param>
        protected virtual void Abort(Pawn baby, string messageKey, params string[] messageArgs) {
            if (messageKey != null && parent.Visible && PawnUtility.ShouldSendNotificationAbout(Pawn)) {
                Messages.Message(messageKey.Translate(messageArgs).CapitalizeFirst(), Pawn, MessageTypeDefOf.NegativeHealthEvent);
            }
            ((Hediff_Pregnancy)parent).Abort(baby);
        }

        /// <summary>
        /// Force an abortion of the current pregnancy. As there are no conditions to check for (yet,) this
        /// method should be called by the recipe, hediff, or other action in order to start
        /// the abortion.
        /// </summary>
        /// <param name="messageKey">a message bundle key to provide the translated message</param>
        /// <param name="messageArgs">a collection of arguments to be passed for formatting the provided message</param>
        public void Abort(string messageKey = null, params string[] messageArgs) {
            foreach (Pawn baby in ((Hediff_Pregnancy)parent).Babies) {
                CLog.Message("Causing abortion of unborn baby " + baby.Name);
                Abort(baby, messageKey, messageArgs);
            }
             ((Hediff_Pregnancy)parent).Destroy();
        }
    }

    public class CompProperties_Abortable : HediffCompProperties {
        public float maxSeverity;

        public CompProperties_Abortable() {
            compClass = typeof(CompAbortable);
        }
    }
}
