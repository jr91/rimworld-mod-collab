﻿namespace RimWorldChildren {
    using System.Collections.Generic;
    using RimWorld;

    [DefOf]
    public static class LifeStageDefOf {
        public static LifeStageDef HumanlikeBaby;
        public static LifeStageDef HumanlikeToddler;
        public static LifeStageDef HumanlikeChild;
        public static LifeStageDef HumanlikeTeenager;
        public static LifeStageDef HumanlikeAdult;

        static LifeStageDefOf() {
            DefOfHelper.EnsureInitializedInCtor(typeof(LifeStageDefOf));
        }

        /// <summary>
        /// Determines if the provided def matches one of the above lifestages
        /// </summary>
        /// <param name="def">any lifestage def to compare</param>
        public static bool IsOfType(LifeStageDef def) {
            return def == HumanlikeBaby || def == HumanlikeToddler || def == HumanlikeChild || def == HumanlikeTeenager || def == HumanlikeAdult;
        }

        /// <summary>
        /// Returns a list of all basic LifeStageDefs
        /// </summary>
        public static List<LifeStageDef> AsList() {
            return new List<LifeStageDef>() { HumanlikeBaby, HumanlikeToddler, HumanlikeChild, HumanlikeTeenager, HumanlikeAdult };
        }
    }
}
