﻿namespace RimWorldChildren {
    using Verse;

    /// <summary>
    /// Model to contain a full collection of MeshSets for pawn rendering
    /// </summary>
    public class MeshsetPackage {
        public GraphicMeshSet bodyMeshSet;
        public GraphicMeshSet headMeshSet;
        public GraphicMeshSet hairMeshSet;
    }
}
