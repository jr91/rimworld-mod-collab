﻿namespace RimWorldChildren {
    using System.Collections.Generic;
    using RimWorld;
    using UnityEngine;
    using Verse;

    /// <summary>
    /// Defines the model for a specific life stage
    /// </summary>
    public class LifecycleStage {
        // Required: It is assumed that a childhood stage will map directly to some life stage
        public LifeStageDef lifeStageDef;

        /// <summary>
        /// Required.
        /// This field is used to broadly flag this life stage as general "type" of age, such as a baby or a child.
        /// This will be used for a number of features that are specific to age ranges, but not necessarily configurable - such
        /// as thought generation.
        ///
        /// Developers should use this field extremely sparingly, as its indicates an inflexible implementation.
        /// </summary>
        public LifestageType lifestageType;

        // Optional: minimum age for this stage to apply
        public int minAge = -1;

        // Optional: A single stage may use this flag to denote that at this stage, the pawn reaches maturity and will acquire any traits they have earned
        public bool maturity = false;

        // Optional: Capacity modifiers that apply to pawns in this stage
        public List<PawnCapacityModifier> capMods = new List<PawnCapacityModifier>();

        // Optional: Backstory to apply at this stage. This will overwrite the previous backstory
        public string backstoryIdentifier;

        // Optional: Pawn cannot receive these thoughts at this age stage
        public List<ThoughtDef> cannotReceiveThoughts;

        // Optional: The maximum weapon mass a pawn at this lifestage can carry safely
        public float? maxWeaponMass;

        // Optional: At this lifestage, this pawn will not alert the player if idle
        public bool idleDisabled = false;

        // Optional: This pawn will wake up if they qualify as "unhappy" based on hediff presence
        public bool wakeOnUnhappy = false;

        // Optional: When true, pawn will prefer to use a crib over a regular bed
        public bool usesCrib = false;

        // Optional: When true, this pawn should be fed by a caretaker pawn when hungry
        public bool shouldBeFed = false;

        // Optional: Subset of rules for displaying different graphics
        public GraphicsRuleset graphics = null;
    }
}
