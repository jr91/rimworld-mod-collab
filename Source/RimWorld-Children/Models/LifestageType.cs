﻿
namespace RimWorldChildren{
    /// <summary>
    /// This enum will be used to flag a lifestage as a more general type to use various features of CNP.
    /// Unborn should be used for pawns still in the womb, "egg" pawns, or other pre-birth pawn types.
    /// </summary>
    public enum LifestageType : byte {
        Unborn,
        Newborn,
        Baby,
        Toddler,
        Child,
        Adolescent,
        Adult,
        Elder,
    }
}
