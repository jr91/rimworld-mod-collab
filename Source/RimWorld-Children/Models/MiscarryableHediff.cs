﻿
namespace RimWorldChildren {
    /// <summary>
    /// This class represents a hediff that can cause a miscarriage once a certain severity is reached
    /// </summary>
    public class MiscarryableHediff {
        public string hediffDef;
        public float minSeverity;
    }
}
