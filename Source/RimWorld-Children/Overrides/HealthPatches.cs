namespace RimWorldChildren {
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Reflection.Emit;
    using HarmonyLib;
    using RimWorld;
    using RimWorldChildren.API;
    using Verse;

    /// <summary>
    /// Harmony patches for classes related to pawn health, surgery, etc
    /// </summary>
    public class PawnHealthTrackerPatches {
        /// <summary>
        /// Forces child pawns to collapse more quickly from wounds
        /// </summary>
        [HarmonyPatch(typeof(Pawn_HealthTracker), "ShouldBeDowned")]
        public static class Pawn_HealtherTracker_ShouldBeDowned_Patch {
            [HarmonyPostfix]
            internal static void SBD(ref Pawn_HealthTracker __instance, ref bool __result) {
                Pawn pawn = (Pawn)AccessTools.Field(typeof(Pawn_HealthTracker), "pawn").GetValue(__instance);
                if (pawn.RaceProps.Humanlike && ChildrenUtility.GetLifestageType(pawn) <= LifestageType.Child) {
                    __result = __instance.hediffSet.PainTotal > 0.4f || !__instance.capacities.CanBeAwake || !__instance.capacities.CapableOf(PawnCapacityDefOf.Moving);
                }
            }
        }
    }
        
    [HarmonyPatch(typeof(HealthAIUtility), "ShouldSeekMedicalRestUrgent")]
    public static class ShouldSeekMedicalRestUrgent_Patch {
        /// <summary>
        /// Combined with RestUtility patches, this patch ensures that a baby pawn who is not
        /// truly in need of medical attention will be moved to a crib instead of rescued
        /// to a hospital bed.
        /// </summary>
        [HarmonyPrefix]
        public static bool ShouldSeekMedicalRestUrgent_Prefix(Pawn pawn, ref bool __result) {
            if (!ChildrenUtility.ShouldUseCrib(pawn) || pawn.InBed()) {
                return true; // Default back to original flow
            }

            __result = true;
            if (!pawn.health.HasHediffsNeedingTend(false)) {
                __result = HealthAIUtility.ShouldHaveSurgeryDoneNow(pawn);
            }

            return false;
        }

        private static bool RecipeHasNoIngredients(RecipeDef recipe) {
            return recipe.ingredients.Count == 0;
        }

        /// <summary>
        /// Fixes null reference exception error if a Bill_Medical has no actual ingredients
        /// Remove this code when it is fixed in Vanilla
        /// </summary>
        [HarmonyPatch(typeof(Bill_Medical), "Notify_DoBillStarted")]
        public static class Bill_Medical_NotifyDoBillStarted_Patch {
            [HarmonyTranspiler]
            static IEnumerable<CodeInstruction> Notify_DoBillStarted_Transpiler(IEnumerable<CodeInstruction> instructions, ILGenerator ILgen) {
                List<CodeInstruction> ILs = instructions.ToList();

                // Add the "jumpToEnd" label onto the last instruction
                Label jumpToEnd = ILgen.DefineLabel();
                ILs.Last().labels.Add(jumpToEnd);
                int index = ILs.FindIndex(IL => IL.opcode == OpCodes.Brtrue) + 1;
                var injection = new List<CodeInstruction> {
                new CodeInstruction(OpCodes.Ldarg_0),
                new CodeInstruction(OpCodes.Ldfld, typeof(Bill_Medical).GetField("recipe", AccessTools.all)),
                new CodeInstruction(OpCodes.Call, typeof(ShouldSeekMedicalRestUrgent_Patch).GetMethod("RecipeHasNoIngredients", AccessTools.all)),
                new CodeInstruction(OpCodes.Brtrue, jumpToEnd),
            };
                ILs.InsertRange(index, injection);
                foreach (CodeInstruction instruction in ILs) {
                    yield return instruction;
                }
            }
        }
    }
}
