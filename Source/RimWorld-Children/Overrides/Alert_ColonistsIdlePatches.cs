﻿
namespace RimWorldChildren {
    using System.Collections.Generic;
    using HarmonyLib;
    using RimWorld;
    using Verse;

    [HarmonyPatch(typeof(Alert_ColonistsIdle), "IdleColonists", MethodType.Getter)]
    public static class Alert_ColonistsIdle_IdleColonists_Patch {
        [HarmonyPostfix]
        public static void IdleColonists_Getter_Patch(ref List<Pawn> __result) {
            __result.RemoveAll(pawn => pawn.TryGetComp<LifecycleComp>()?.CurrentLifestage?.idleDisabled ?? false);
        }
    }
}
