﻿namespace RimWorldChildren {
    using System.Collections.Generic;
    using System.Reflection.Emit;
    using HarmonyLib;
    using RimWorldChildren.API;
    using UnityEngine;
    using Verse;

    public class PawnCapacityUtilityPatches {
        [HarmonyPatch(typeof(PawnCapacityUtility), "CalculateCapacityLevel")]
        public static class CalculateCapacityLevel_Patch {

            /// <summary>
            /// Factor in lifestage capacity modifiers when determining pawn capacity.
            /// </summary>
            [HarmonyPostfix]
            public static void CalculateCapacityLevel_Postfix(HediffSet diffSet, PawnCapacityDef capacity, ref float __result) {
                LifecycleComp comp = diffSet?.pawn?.TryGetComp<LifecycleComp>() ?? null;
                CompCarryChild carryComp = diffSet?.pawn?.TryGetComp<CompCarryChild>() ?? null;
                if (comp != null && ChildrenUtility.RaceUsesChildren(diffSet.pawn) && comp.CurrentLifestage.capMods.Count > 0) {
                    List<PawnCapacityModifier> capMods = comp.CurrentLifestage.capMods;
                    ApplyCapMods(capMods, capacity, ref __result);
                } else if (carryComp != null && carryComp.Carried != null) {
                    List<PawnCapacityModifier> capMods = CompCarryChild.capMods;
                    ApplyCapMods(capMods, capacity, ref __result);
                }
            }

            private static void ApplyCapMods(List<PawnCapacityModifier> capMods, PawnCapacityDef capacity, ref float __result) {
                float capacityValue = __result;
                if (__result > 0.0) {
                    float max = __result;
                    float capacityMult = 1f;
                    if (capMods != null) {
                        for (int i = 0; i < capMods.Count; ++i) {
                            PawnCapacityModifier capacityModifier = capMods[i];
                            if (capacityModifier.capacity == capacity) {
                                capacityValue += capacityModifier.offset;
                                capacityMult *= capacityModifier.postFactor;
                                float setMax = capacityModifier.setMax;
                                if (setMax < max) {
                                    max = setMax;
                                }
                            }
                        }
                    }

                    __result = Mathf.Min(capacityValue * capacityMult, max);
                }
            }
        }
    }
}
