﻿namespace RimWorldChildren {
    using System;
    using System.Reflection;
    using HarmonyLib;
    using Verse;

    /// <summary>
    /// Moving this from ChildrenAndPregnancy as the static constructor there is executed too early
    /// in the game's initialization.
    /// </summary>
    [StaticConstructorOnStartup]
    class HarmonyPatcher {
        static HarmonyPatcher() {
            var harmony = new Harmony("children.and.pregnancy");
            harmony.PatchAll();
            if (ModInterface.HumanoidAlienRaces) {
                harmony.Patch(AccessTools.Method(AccessTools.TypeByName("AlienRace.HarmonyPatches"), "DrawAddons"), null, null, new HarmonyMethod(typeof(AlienRaces_DrawAddons_Patch), "DrawAddons_Transpiler"));
                harmony.Patch(AccessTools.Method(AccessTools.TypeByName("BodyAddon"), "CanDrawAddon"), new HarmonyMethod(typeof(AlienRaces_BodyAddon_Patch), "CanDrawAddon_Prefix"), null, null);
                harmony.Patch(AccessTools.Method(AccessTools.TypeByName("AlienRace.AlienPartGenerator"), "GetNakedPath"), new HarmonyMethod(typeof(AlienRaces_GetNakedPath_Patch), "GetNakedPath_Prefix"), null, null);
            }
        }
    }
}
