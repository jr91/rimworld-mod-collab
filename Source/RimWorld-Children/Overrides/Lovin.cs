﻿namespace RimWorldChildren {
    using System;
    using System.Collections.Generic;
    using System.Reflection;
    using HarmonyLib;
    using RimWorld;
    using RimWorldChildren.API;
    using Verse;
    using Verse.AI;

    /// <summary>
    ///  Patches for classes related to lovin'
    /// </summary>
    [HarmonyPatch(typeof(JobDriver), "Cleanup")]
    internal static class Lovin_Override
    {
        [HarmonyPrefix]
        internal static bool JobDriver_Lovin_Patch(JobDriver __instance, JobCondition condition) {
            if (__instance == null) {
                return true;
            }

            if (__instance.GetType() == typeof(JobDriver_Lovin) && condition == JobCondition.Succeeded) {
                Pawn partner = (Pawn)__instance.GetType().GetProperty("Partner", BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic).GetValue(__instance, null);
                Pawn pawn = __instance.pawn;
                TryToImpregnate(pawn, partner);
            }

            return true;
        }

        internal static void TryToImpregnate(Pawn initiator, Pawn partner) {
            if (!PregnancyUtility.ColonyCanGiveBirth() || !PregnancyUtility.CanPawnsMate(initiator, partner)) {
                return;
            }

            Pawn male = initiator.gender == Gender.Male ? initiator : partner;
            Pawn female = initiator.gender == Gender.Female ? initiator : partner;

            // Check the pawn's age to see how likely it is she can carry a fetus
            // 25 and below is guaranteed, 50 and above is impossible, 37.5 is 50% chance
            float preg_chance = Math.Max (1 - (Math.Max (female.ageTracker.AgeBiologicalYearsFloat - 25, 0) / 25), 0) * female.TryGetComp<LifecycleComp>().LifecycleDef.impregnationChance;

            if (preg_chance < Rand.Value) {
                CLog.Message("Impregnation failed. Chance was " + preg_chance);
                return;
            }

            CLog.Message("Impregnation succeeded. Chance was " + preg_chance);

            // Spawn a bunch of hearts. Sharp eyed players may notice this means impregnation occurred.
            for (int i = 0; i <= 3; i++) {
                MoteMaker.ThrowMetaIcon(male.Position, male.MapHeld, ThingDefOf.Mote_Heart);
                MoteMaker.ThrowMetaIcon(female.Position, male.MapHeld, ThingDefOf.Mote_Heart);
            }

            PregnancyUtility.ImpregnatePawn<Hediff_HumanPregnancy>(female, male);
        }
    }

    [HarmonyPatch(typeof(JobGiver_DoLovin), "TryGiveJob")]
    public static class JobGiver_DoLovin_TryGiveJob_Patch {
        [HarmonyPostfix]
        internal static void TryGiveJob_Patch(ref Job __result, ref Pawn pawn) {
            if (pawn.ageTracker != null && ChildrenUtility.GetLifestageType(pawn) <= LifestageType.Child) {
                __result = null;
            }
        }
    }
}