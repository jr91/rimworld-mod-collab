﻿namespace RimWorldChildren {
    using System.Collections.Generic;
    using RimWorld;
    using RimWorldChildren.API;
    using Verse;
    using Verse.AI;

    public class WorkGiver_TakeBabyToCrib : WorkGiver_Scanner {
        public override PathEndMode PathEndMode {
            get {
                return PathEndMode.Touch;
            }
        }

        public override IEnumerable<Thing> PotentialWorkThingsGlobal(Pawn pawn) {
            return pawn.Map.mapPawns.SpawnedDownedPawns;
        }

        public override bool ShouldSkip(Pawn pawn, bool forced = false) {
            List<Pawn> pawnList = pawn.Map.mapPawns.AllPawnsSpawned;
            for (int index = 0; index < pawnList.Count; ++index) {
                if (pawnList[index].guest?.HostFaction == pawn.Faction || pawnList[index].Faction == pawn.Faction) {
                    // Atleast one pawn who ought to be in a crib
                    if (ChildrenUtility.ShouldUseCrib(pawnList[index]) && !pawnList[index].InBed()) {
                        return false;
                    }
                }
            }

            return true;
        }

        public override bool HasJobOnThing (Pawn pawn, Thing t, bool forced = false) {
            if (t == null || t == pawn) {
                return false;
            }

            Pawn baby = t as Pawn;
            if (baby.Faction != pawn.Faction && !(baby.guest.HostFaction != pawn.Faction)) {
                return false;
            }

            if (!ChildrenUtility.RaceUsesChildren(baby) || !baby.RaceProps.Humanlike) {
                return false;
            }

            if (!ChildrenUtility.ShouldUseCrib(baby)) {
                return false;
            }

            if (!pawn.CanReserveAndReach(baby, PathEndMode.ClosestTouch, Danger.Deadly, 1, -1, null, forced)) {
                return false;
            }

            Building_Bed crib = RestUtility.FindBedFor(baby, pawn, baby.IsPrisoner, false);
            if (crib == null) {
                JobFailReason.Is("NoCrib".Translate());
                return false;
            }

            // If not in a crib, but can't find a crib to go to
            if (baby.InBed() && (ChildrenUtility.IsBedCrib(baby.CurrentBed()) || !ChildrenUtility.IsBedCrib(crib))) {
                return false;
            }

            if (!pawn.CanReserveAndReach(crib, PathEndMode.ClosestTouch, Danger.Deadly, 1, -1, null, forced)) {
                return false;
            }

            // Is it time for the baby to go to bed?
            if (!baby.timetable.GetAssignment(GenLocalDate.HourInteger(baby.Map)).allowRest) {
                return false;
            }

            return true;
        }

        public override Job JobOnThing(Pawn pawn, Thing t, bool forced = false) {
            var baby = (Pawn)t;
            Building_Bed crib = RestUtility.FindBedFor(baby, pawn, baby.IsPrisoner, false);
            if (baby != null && crib != null) {
                Job carryBaby = new Job(DefDatabase<JobDef>.GetNamed("TakeBabyToCrib"), baby, crib) { count = 1 };
                return carryBaby;
            }

            return null;
        }
    }
}