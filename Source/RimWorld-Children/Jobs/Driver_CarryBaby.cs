﻿namespace RimWorldChildren {
    using System.Collections.Generic;
    using RimWorld;
    using RimWorldChildren;
    using RimWorldChildren.API;
    using Verse;
    using Verse.AI;

    /// <summary>
    /// This job will allow a pawn to pick up and carry a small child.
    /// </summary>
    public class Driver_CarryBaby : JobDriver {
        private Pawn Child {
            get {
                return (Pawn)TargetThingA;
            }
        }

        public override bool TryMakePreToilReservations(bool errorOnFailed) {
            return this.pawn.Reserve(Child, this.job, 1, -1, null, errorOnFailed);
        }

        protected override IEnumerable<Toil> MakeNewToils() {
            this.FailOnDespawnedOrNull(TargetIndex.A);
            yield return Toils_Goto.GotoThing(TargetIndex.A, PathEndMode.Touch);
            Toil carry = new Toil();
            carry.initAction = delegate {
                Child.jobs.StopAll();
                Job carryJob = new Job(CnpJobDefOf.Carried, pawn);
                carryJob.count = 1;
                Child.jobs.TryTakeOrderedJob(carryJob);
                ReadyForNextToil();
            };
            yield return carry;
        }
    }
}
