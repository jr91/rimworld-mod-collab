﻿namespace RimWorldChildren {
    using System.Collections.Generic;
    using System.Diagnostics;
    using RimWorld;
    using RimWorldChildren.API;
    using Verse;
    using Verse.AI;

    public class WorkGiver_BreastfeedBaby : WorkGiver_Scanner {
        public override PathEndMode PathEndMode {
            get {
                return PathEndMode.Touch;
            }
        }

        public override IEnumerable<Thing> PotentialWorkThingsGlobal(Pawn pawn) {
            return pawn.Map.mapPawns.SpawnedHungryPawns;
        }

        public override bool HasJobOnThing (Pawn pawn, Thing t, bool forced = false) {
            Pawn baby = t as Pawn;
            if (baby == null || baby == pawn) {
                return false;
            }

            if (!baby.RaceProps.Humanlike || !ChildrenUtility.RaceUsesChildren(baby)) {
                return false;
            }

            if (baby.needs.food == null || baby.needs.food.CurLevelPercentage > baby.needs.food.PercentageThreshHungry + 0.02) {
                return false;
            }

            if (!baby.InBed()) {
                return false;
            }

            if (!ChildrenUtility.ShouldBeFed(baby)) {
                return false;
            }

            if (!pawn.CanReserveAndReach(t, PathEndMode.ClosestTouch, Danger.Deadly, 1, -1, null, forced)) {
                return false;
            }

            if (!ChildrenUtility.CanBreastfeed(pawn)) {
                JobFailReason.Is("ReasonCannotBreastfeed".Translate(pawn.LabelShort));
                return false;
            }

            return true;
        }

        public override Job JobOnThing(Pawn pawn, Thing t, bool forced = false) {
            Pawn pawn2 = (Pawn)t;
            if (pawn2 != null) {
                if (ChildrenUtility.CanBreastfeed(pawn)) {
                    return new Job(DefDatabase<JobDef>.GetNamed("BreastfeedBaby")) {
                        targetA = pawn2,
                    };
                }
            }

            return null;
        }
    }

    public class JobDriver_BreastfeedBaby : JobDriver {
        private const int breastFeedDuration = 600;

        protected Pawn Baby {
            get {
                return (Pawn)TargetA.Thing;
            }
        }

        public override bool TryMakePreToilReservations(bool errorOnFailed) {
            return pawn.Reserve(Baby, job, 1, -1, null);
        }

        [DebuggerHidden]
        protected override IEnumerable<Toil> MakeNewToils()  {
            this.FailOnDespawnedOrNull (TargetIndex.A);
            this.FailOnSomeonePhysicallyInteracting (TargetIndex.A);
            this.FailOn(delegate {
                return !ChildrenUtility.CanBreastfeed(pawn) || !pawn.CanReserve(TargetA, 1, -1, null, false);
            });

            yield return Toils_Reserve.Reserve (TargetIndex.A, 1, -1, null);
            yield return Toils_Goto.GotoThing (TargetIndex.A, PathEndMode.Touch);
            Toil prepare = new Toil();
            prepare.initAction = delegate {
                if (ChildrenUtility.GetLifestageType(Baby) > LifestageType.Baby) {
                    PawnUtility.ForceWait(Baby, breastFeedDuration, Baby);
                }
            };
            prepare.defaultCompleteMode = ToilCompleteMode.Delay;
            prepare.defaultDuration = breastFeedDuration;
            yield return prepare;
            yield return new Toil {
                initAction = delegate {
                    AddEndCondition (() => JobCondition.Succeeded);

                    // Baby is full
                    Baby.needs.food.CurLevelPercentage = 1f;
                    Baby.needs.mood.thoughts.memories.TryGainMemory(ThoughtDef.Named("GotFed"), null);
                },
                defaultCompleteMode = ToilCompleteMode.Instant,
            };
        }
    }

    public class WorkGiver_FoodFeedBaby : WorkGiver_Scanner {
        public override PathEndMode PathEndMode {
            get {
                return PathEndMode.Touch;
            }
        }

        public override ThingRequest PotentialWorkThingRequest {
            get {
                return ThingRequest.ForGroup(ThingRequestGroup.Pawn);
            }
        }

        public override bool HasJobOnThing(Pawn pawn, Thing t, bool forced = false) {
            Pawn baby = t as Pawn;
            if (baby == null || baby == pawn) {
                return false;
            }

            if (!baby.RaceProps.Humanlike || ChildrenUtility.GetLifestageType(baby) > LifestageType.Toddler) {
                return false;
            }

            if (baby.needs.food == null || baby.needs.food.CurLevelPercentage > baby.needs.food.PercentageThreshHungry + 0.02) {
                return false;
            }

            if (!baby.InBed()) {
                return false;
            }

            if (!ChildrenUtility.ShouldBeFed(baby)) {
                return false;
            }

            if (!pawn.CanReserveAndReach(t, PathEndMode.ClosestTouch, Danger.Deadly, 1, -1, null, forced)) {
                return false;
            }

            if (ChildrenUtility.CanBreastfeed(pawn))  {
                return false;
            }

            Thing thing;
            ThingDef thingDef;
            if (!FoodUtility.TryFindBestFoodSourceFor(pawn, baby, baby.needs.food.CurCategory == HungerCategory.Starving, out thing, out thingDef, false)) {
                JobFailReason.Is("NoFood".Translate(), null);
                return false;
            }

            return true;
        }

        public override Job JobOnThing(Pawn pawn, Thing t, bool forced = false) {
            var baby = (Pawn)t;

            if (!baby.InBed()) {
                return null;
            }

            Thing thing;
            ThingDef thingDef;
            Thing foodInInv = FoodUtility.BestFoodInInventory(pawn, baby, FoodPreferability.MealSimple);
            if (foodInInv == null) {
                FoodUtility.TryFindBestFoodSourceFor(pawn, baby, baby.needs.food.CurCategory == HungerCategory.Starving, out thing, out thingDef, false);
            }
            else {
                thing = foodInInv;
                thingDef = thing.def;
            }

            if (thing != null) {
                float nutrition = FoodUtility.GetNutrition(thing, thingDef);
                var feedBaby = new Job(DefDatabase<JobDef>.GetNamed("FoodFeedBaby"), thing, baby) {
                    count = FoodUtility.WillIngestStackCountOf(baby, thingDef, nutrition),
                };
                baby.needs.mood.thoughts.memories.TryGainMemory(ThoughtDef.Named("GotFed"), null);
                return feedBaby;
            }

            return null;
        }
    }
}