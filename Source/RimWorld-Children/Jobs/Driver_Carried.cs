﻿namespace RimWorldChildren {
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Verse;
    using Verse.AI;

    public class Driver_Carried : JobDriver {
        public Pawn Carrier {
            get {
                return job.targetA.Thing as Pawn;
            }
        }

        protected override IEnumerable<Toil> MakeNewToils() {
            yield return BeCarried();
        }

        public override bool TryMakePreToilReservations(bool errorOnFailed) {
            return pawn.Reserve(pawn, job, 1, -1, null);
        }

        public override bool CanBeginNowWhileLyingDown() {
            return true;
        }

        private Toil BeCarried() {
            Toil toil = new Toil();
            toil.defaultCompleteMode = ToilCompleteMode.Never;
            toil.initAction = delegate {
                pawn.TryGetComp<CompCarryChild>().Carrier = Carrier;
                Carrier.TryGetComp<CompCarryChild>().Carried = pawn;
            };

            toil.tickAction = delegate {
                if (ShouldDrop()) {
                    ReadyForNextToil();
                    return;
                }

                pawn.Drawer.tweener = Carrier.Drawer.tweener;
                pawn.Position = Carrier.Position;
                pawn.Rotation = Carrier.Rotation;
            };

            toil.AddFinishAction(delegate {
                pawn.Drawer.tweener = new PawnTweener(pawn);
                pawn.pather.ResetToCurrentPosition();
                pawn.TryGetComp<CompCarryChild>().Reset();
                Carrier.TryGetComp<CompCarryChild>().Reset();
            });

            return toil;
        }

        /// <summary>
        /// Determines if the carry relationship should be broken based on the status of the carrier and carried.
        /// </summary>
        /// <returns>True if the carried pawn should be dropped.</returns>
        private bool ShouldDrop() {
            return Carrier == null || Carrier.Downed || Carrier.Dead || !Carrier.Spawned || !Carrier.Drafted || pawn.Drafted || Carrier.TryGetComp<CompCarryChild>().ShouldDrop 
                   || pawn.TryGetComp<CompCarryChild>().ShouldDrop;
        }
    }
}
