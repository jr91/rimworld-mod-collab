﻿using CombatExtended;
using HarmonyLib;
using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using Verse;

namespace RimWorldChildren {
    /// <summary>
    /// A class to handle graphics adjustment prior to rendering.
    /// </summary>
    public class GraphicsPreloader {
        /// <summary>
        /// Apparel graphics are stored in memory as they are requested, with two levels of caching. The first
        /// level is the graphic itself, which contains miscellaneous metadata that we don't particularly care about.
        /// The second level - contained within a Graphic_Multi - is a Materials cache, which stores every unique loaded
        /// Material. Since scaling and offsetting a Material doesn't make it unique, we need
        /// to make a new copy and assign it to the materials cache stored in the Graphic. This allows us to
        /// adjust our unique Material instance without interfering with the same Material for another pawn.
        /// </summary>
        /// <param name="apparel">The apparel whose graphic should be modified</param>
        /// <param name="pawn">The pawn who defines the graphics rules</param>
        public static void PreloadApparelGraphic(Apparel apparel, Pawn pawn) {
            LifecycleComp comp = pawn.TryGetComp<LifecycleComp>();

            // No need to modify if we have no special graphics rules, or if the apparel has no graphic
            if (comp?.CurrentLifestage?.graphics?.clothesBodyType != null && !string.IsNullOrEmpty(apparel.def.apparel.wornGraphicPath)) {
                if (!(apparel.def.apparel.LastLayer == ApparelLayerDefOf.Overhead || PawnRenderer.RenderAsPack(apparel) || apparel.def.apparel.wornGraphicPath == BaseContent.PlaceholderImagePath)) {
                    if (ModInterface.CombatExtended && CheckCombatExtended(apparel)) {
                        return;
                    }

                    string realPath = apparel.def.apparel.wornGraphicPath + "_" + pawn.story.bodyType.defName;
                    string mockedPath = apparel.def.apparel.wornGraphicPath + "_" + comp.CurrentLifestage.graphics.clothesBodyType.defName;
                    Shader shader = apparel.def.apparel.useWornGraphicMask ? ShaderDatabase.CutoutComplex : ShaderDatabase.Cutout;

                    // To load the database, we will set our real request (with the child's body type) as the key, but the mocked request's value as the value
                    // This will ensure when something requests a child body graphic (which may not exist,) we will instead pull back the clothesBodyType graphic
                    GraphicRequest realReq = new GraphicRequest(typeof(Graphic_Multi), realPath, shader, apparel.def.graphicData.drawSize, (Color32)apparel.DrawColor, (Color32)Color.white, null, 0, null); // this cast to Color32 is essential for some God forsaken reason
                    GraphicRequest mockedReq = new GraphicRequest(typeof(Graphic_Multi), mockedPath, shader, apparel.def.graphicData.drawSize, (Color32)apparel.DrawColor, (Color32)Color.white, null, 0, null);
                    Traverse graphicsDbTraverse = Traverse.Create(typeof(GraphicDatabase));
                    Dictionary<GraphicRequest, Graphic> allGraphics = graphicsDbTraverse.Field("allGraphics").GetValue<Dictionary<GraphicRequest, Graphic>>();
                    if (!allGraphics.ContainsKey(realReq)) {
                        Graphic newGraphic = new Graphic_Multi();
                        newGraphic.Init(mockedReq);

                        // Get and reassign new materials for the graphic so we do not override existing materials
                        Material[] mats = (Material[])Traverse.Create(newGraphic).Field("mats").GetValue();
                        mats[0] = AdjustMaterial(mats[0], comp, Rot4.North);
                        mats[1] = AdjustMaterial(mats[1], comp, Rot4.East);
                        mats[2] = AdjustMaterial(mats[2], comp, Rot4.South);
                        mats[3] = AdjustMaterial(mats[3], comp, Rot4.West);

                        allGraphics.Add(realReq, newGraphic);
                    }
                }
            }
        }

        private static Material AdjustMaterial(Material mat, LifecycleComp comp, Rot4 rot) {
            Material newMat = new Material(mat);
            newMat.mainTextureScale = comp.CurrentLifestage.graphics.clothingScale;
            if (rot == Rot4.West || rot == Rot4.East) {
                newMat.mainTextureOffset = comp.CurrentLifestage.graphics.clothingOffsetEW;
            }
            else {
                newMat.mainTextureOffset = comp.CurrentLifestage.graphics.clothingOffsetNS;
            }

            return newMat;
        }

        private static bool CheckCombatExtended(Apparel apparel) {
            return apparel.def.apparel.LastLayer.GetModExtension<ApparelLayerExtension>()?.IsHeadwear ?? false;
        }
    }
}
