﻿
namespace RimWorldChildren {
    using Verse;

    /// <summary>
    /// Wrapper for Log to add our prefix and give us write options in case we want to disable log messages
    /// </summary>
    public class CLog {
        public static void Message(string message) {
            if (ChildrenAndPregnancy.Settings.debugLogging) {
                Log.Message("[CNP]" + message);
            }
        }

        public static void Warning(string message) {
            Log.Warning("[CNP]" + message);
        }

        public static void Error(string message) {
            Log.Warning("[CNP]" + message);
        }
    }
}
